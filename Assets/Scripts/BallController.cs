﻿using System;
using UnityEngine;
using static GameEnums;

public class BallController : MonoBehaviour
{
	public static event Action<BallType> OnBallStartMoving = delegate { };
	public static event Action<BallType> OnBallStopMoving = delegate { };
	public static event Action<BallType> OnBallHit = delegate { };
	public static event Action<SoundType, float> OnPlaySound = delegate { };

	[SerializeField]
	private BallType ballType;

	private Rigidbody2D myRigidBody;
	private bool isMoving = false;
	float hitForce = 0f;
	bool isPowering = false;

	void Awake ()
	{
		myRigidBody = GetComponent<Rigidbody2D> ();
	}

	void Update()
    {
		if (ballType == BallType.Player && !GameManager.Instance.IsAnyBallMoving ())
		{
			HandleInput ();
		}

		//hard stop the ball if it is moving very slow
		if (isMoving && myRigidBody.velocity.magnitude < 0.05f)
		{
			myRigidBody.velocity = Vector2.zero;
			isMoving = false;
			OnBallStopMoving (ballType);
		}
	}

	private void HandleInput ()
	{
		//init aming on key press down
		if (Input.GetKeyDown (KeyCode.Space))
		{
			isPowering = true;
			GameManager.Instance.MyTrajectoryRenderer.gameObject.SetActive (true);
			MenuManager.Instance.ShowShotPowerDisplayAt (transform.position);
		}
		//increase hit power and update direction while key pressed
		if (Input.GetKey (KeyCode.Space) && isPowering)
		{
			hitForce += Time.deltaTime * 3f;
			hitForce = Mathf.Min (hitForce, 10);
			GameManager.Instance.MyTrajectoryRenderer.DrawTrajectory (transform.position, GetCurrentAimDirection (), 1f, 0.05f, 7f);
			MenuManager.Instance.UpdateShotPowerDisplay (hitForce);
		}

		//apply force on key release
		if (Input.GetKeyUp (KeyCode.Space) && isPowering)
		{
			myRigidBody.AddForce (GetCurrentAimDirection () * hitForce, ForceMode2D.Impulse);
			isMoving = true;
			OnBallStartMoving (ballType);
			isPowering = false;
			hitForce = 0.1f;
			GameManager.Instance.MyTrajectoryRenderer.gameObject.SetActive (false);
			MenuManager.Instance.HideShotPowerDisplay ();
		}
	}

	private void OnCollisionEnter2D (Collision2D collision)
	{
		float impulse = 0f;
		foreach (ContactPoint2D cp in collision.contacts)
		{
			impulse += cp.normalImpulse;
		}
		
		var otherBall = collision.gameObject.GetComponent<BallController> ();
		if (otherBall)
		{
			OnPlaySound (SoundType.BallHit, impulse);

			//player hits ball
			if (ballType == BallType.Player)
			{
				OnBallHit (otherBall.ballType);
			}

			if (!isMoving)
			{
				isMoving = true;
				OnBallStartMoving (ballType);
			}
		}
		//if ball hits cushion
		else if (collision.gameObject.CompareTag ("Cushion"))
		{
			OnPlaySound (SoundType.CushionHit, impulse);
		}
	}

	private Vector3 GetCurrentAimDirection ()
	{
		var vec = (GetCurrentWorldMousePos () - transform.position);
		vec.Normalize ();
		return vec;
	}

	private Vector3 GetCurrentWorldMousePos ()
	{
		var correctMousePos = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0f);
		var pos = Camera.main.ScreenToWorldPoint (correctMousePos);
		pos.z = 0f;
		return pos;
	}
}
