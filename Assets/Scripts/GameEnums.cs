﻿
public class GameEnums
{
	public enum SoundType
	{
		BallHit,
		CushionHit
	}

	public enum BallType
	{
		Player,
		YellowBall,
		RedBall
	}
}
