﻿using System;
using System.Collections;
using UnityEngine;
using static GameEnums;

public class GameManager : MonoBehaviour
{
	[Serializable]
	public struct GameSessionData
	{
		public int shotsMade;
		public float timePassed;
	}

	#region serialized fields

	[SerializeField]
	private int ScoreToFinishGame = 3;

	[SerializeField, Header("Balls")]
	private Rigidbody2D PlayerBall;

	[SerializeField]
	private Transform PlayerStartPos;

	[SerializeField]
	private Rigidbody2D RedBall;

	[SerializeField]
	private Transform RedBallStartPos;

	[SerializeField]
	private Rigidbody2D YellowBall;

	[SerializeField]
	private Transform YellowBallStartPos;

	[SerializeField, Header("Sound")]
	private AudioSource audioSource;

	[SerializeField]
	private AudioClip ballHitSound;

	[SerializeField]
	private AudioClip cushionHitSound;

	[SerializeField]
	private float maseterVolume = 1f;

	[SerializeField, Header("Other stuff")]
	private TrajectoryRenderer trajectoryRenderer;

	[SerializeField]
	private GhostSystem ghostSystem;
	#endregion

	#region private fields

	private bool yellowBallHit, yelloBallMoving, redBallHit, redBallMoving, playerBallMoving;
	private int score;
	private int shots;
	private float passedGameTime;

	private bool gameRuns = false;
	private bool replayInProgress = false;

	private DateTime recordingStartTimeStamp;
	private double lastRecordingDuration = 0f;
	Coroutine replayTracker = null;

	#endregion

	#region properties

	public static GameManager Instance
	{
		get; private set;
	}

	public int CurretnShotsMade
	{
		get
		{
			return shots;
		}
		set
		{
			shots = value;
			MenuManager.Instance.UpdateShotsMadeDisplay (value);
		}
	}

	public int CurrentScore
	{
		get
		{
			return score;
		}
		set
		{
			score = value;
			MenuManager.Instance.UpdateScoreDisplay (value);
		}
	}

	public float CurrentSessionTime
	{
		get
		{
			return passedGameTime;
		}
		set
		{
			passedGameTime = value;
			MenuManager.Instance.UpdateTimePassedDisplay (value);
		}
	}

	public TrajectoryRenderer MyTrajectoryRenderer
	{
		get
		{
			return trajectoryRenderer;
		}
	}
	#endregion

	void Awake()
    {
		if (Instance == null)
		{
			Instance = this;
		}

		MenuManager.Instance.ShowMainMenu ();

		BallController.OnBallHit += OnBallHitEventRecieved;
		BallController.OnBallStartMoving += OnBallStartMovingEventReceived;
		BallController.OnBallStopMoving += OnBallStopMovingEventReceived;
		BallController.OnPlaySound += OnPlaySoundEventRecieved;
    }

	void Update()
    {
		if (gameRuns && !replayInProgress)
		{
			CurrentSessionTime += Time.deltaTime;
		}
	}

	public void StartReplay ()
	{
		ghostSystem.StartReplay ();
		replayTracker = StartCoroutine (ReplayTracker());
		replayInProgress = true;
	}

	public void StartNewGame ()
	{
		MenuManager.Instance.ShowHUDMenu ();
		ResetStatistics ();
		ResetBalls ();
		ResetReplayLogic ();
		gameRuns = true;
	}

	public void OnMasterVolumeChanged (float volume)
	{
		maseterVolume = volume;
	}

	public void QuitApp ()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.ExitPlaymode ();
		#else
			Application.Quit ();
		#endif
	}

#region sound events handling

	private void OnPlaySoundEventRecieved (SoundType soundType, float hitForce)
	{
		if (soundType == SoundType.BallHit)
		{
			audioSource.clip = ballHitSound;
		}
		else
		{
			audioSource.clip = cushionHitSound;
		}

		//map hitForce interval [0.2,10] to volume interval [0.2, 1]
		var volumeHit = (0.09f * hitForce + 0.1f);
		audioSource.volume = maseterVolume* volumeHit;
		audioSource.Play ();
	}
#endregion

#region hanling ball movement events

	private void OnBallHitEventRecieved (BallType ballType)
	{
		if (ballType == BallType.RedBall)
		{
			redBallHit = true;
		}
		if (ballType == BallType.YellowBall)
		{
			yellowBallHit = true;
		}
		CheckForScore ();
	}

	private void OnBallStartMovingEventReceived (BallType ballType)
	{
		if (ballType == BallType.YellowBall)
		{
			yelloBallMoving = true;
		}
		else if (ballType == BallType.RedBall)
		{
			redBallMoving = true;
		}
		else if (ballType == BallType.Player)
		{
			playerBallMoving = true;
			CurretnShotsMade++;
			recordingStartTimeStamp = DateTime.Now;
			ghostSystem.StartRecording ();
			MenuManager.Instance.SetReplayButtonEnabled (false);
		}
	}

	private void OnBallStopMovingEventReceived (BallType ballType)
	{
		if (ballType == BallType.YellowBall)
		{
			yelloBallMoving = false;
		}
		else if (ballType == BallType.RedBall)
		{
			redBallMoving = false;
		}
		else if (ballType == BallType.Player)
		{
			playerBallMoving = false;
		}
		CheckAllBallsStopMoving ();
	}

#endregion

#region helper functions

	private void CheckForScore ()
	{
		if (yellowBallHit && redBallHit)
		{
			yellowBallHit = false;
			redBallHit = false;

			CurrentScore++;
			if (CurrentScore >= ScoreToFinishGame)
			{
				//finish game logic
				gameRuns = false;
				MenuManager.Instance.ShowFinishMenu (CurretnShotsMade, CurrentSessionTime);
							
				var sessionData = new GameSessionData ();
				sessionData.shotsMade = shots;
				sessionData.timePassed = passedGameTime;
				PlayerPrefs.SetString ("lastSessionResult", JsonUtility.ToJson (sessionData));
			}
		}
	}

	private void CheckAllBallsStopMoving ()
	{
		if (!IsAnyBallMoving())
		{
			yellowBallHit = false;
			redBallHit = false;
			MenuManager.Instance.SetReplayButtonEnabled (true);
			ghostSystem.StopRecording ();
			lastRecordingDuration = (DateTime.Now - recordingStartTimeStamp).TotalSeconds;
		}
	}

	public bool IsAnyBallMoving()
	{
		if (yelloBallMoving || redBallMoving || playerBallMoving)
			return true;
		return false;
	}

	IEnumerator ReplayTracker ()
	{
		yield return new WaitForSecondsRealtime ((float)lastRecordingDuration);
		ghostSystem.StopReplay ();
		replayInProgress = false;
		replayTracker = null;
	}

	private void ResetReplayLogic ()
	{
		replayInProgress = false;
		ghostSystem.StopRecording ();
		ghostSystem.StopReplay ();

		if (replayTracker != null)
		{
			StopCoroutine (replayTracker);
			replayTracker = null;
		}
	}

	private void ResetBalls ()
	{
		PlayerBall.velocity = Vector2.zero;
		RedBall.velocity = Vector2.zero;
		YellowBall.velocity = Vector2.zero;
		PlayerBall.transform.position = PlayerStartPos.position;
		RedBall.transform.position = RedBallStartPos.position;
		YellowBall.transform.position = YellowBallStartPos.position;
		yellowBallHit = yelloBallMoving = false;
		redBallHit = redBallMoving = false;
		playerBallMoving = false;
	}

	private void ResetStatistics ()
	{
		CurrentScore = 0;
		CurretnShotsMade = 0;
		CurrentSessionTime = 0f;
	}
#endregion
}
