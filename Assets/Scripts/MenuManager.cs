﻿using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class MenuManager : MonoBehaviour
{
	public static MenuManager Instance
	{
		get; private set;
	}

	[SerializeField, Header("MainMenu")]
	private GameObject MainMenu;

	[SerializeField]
	private Text LastGameResultText;

	[SerializeField, Header ("HUDMenu")]
	private GameObject HUD;

	[SerializeField]
	private Text timeValue;

	[SerializeField]
	private Text shotsValue;

	[SerializeField]
	private Text scoreValue;

	[SerializeField]
	private Text ShotPowerText;

	[SerializeField]
	private GameObject FinishMenu;

	[SerializeField, Header ("FinishMenu")]
	private Text EndResultText;

	[SerializeField]
	private Button replayButton;


	private void Awake ()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}


	#region MainMenu methods

	public void ShowMainMenu ()
	{
		MainMenu.SetActive (true);
		HUD.SetActive (false);
		FinishMenu.SetActive (false);
		if (PlayerPrefs.HasKey ("lastSessionResult"))
		{
			var sessionData = JsonUtility.FromJson<GameSessionData> (PlayerPrefs.GetString ("lastSessionResult"));

			LastGameResultText.text = string.Format ("Last game you needed: {0} shots to finish for {1} seconds", sessionData.shotsMade, sessionData.timePassed);
		}
	}
	#endregion

	#region HUDMenu methods

	public void ShowHUDMenu ()
	{
		HUD.SetActive (true);
		MainMenu.SetActive (false);
		FinishMenu.SetActive (false);
	}

	public void UpdateTimePassedDisplay(float newTime)
	{
		int minutes = (int)newTime / 60;
		int seconds = (int)newTime - 60 * minutes;

		timeValue.text = (string.Format ("{0:0}:{1:00}", minutes, seconds));
	}

	public void UpdateShotsMadeDisplay (int newShotsCount)
	{
		shotsValue.text = newShotsCount.ToString ();
	}

	public void UpdateScoreDisplay (int newScore)
	{
		scoreValue.text = newScore.ToString ();
	}
	
	public void SetReplayButtonEnabled (bool enabled)
	{
		replayButton.interactable = enabled;
	}

	public void ShowShotPowerDisplayAt (Vector3 position)
	{
		ShotPowerText.gameObject.SetActive (true);
		ShotPowerText.transform.position = Camera.main.WorldToScreenPoint (position);
	}

	public void HideShotPowerDisplay ()
	{
		ShotPowerText.gameObject.SetActive (false);
	}

	public void UpdateShotPowerDisplay (float shotPower)
	{
		ShotPowerText.text = string.Format ("{0:0.00}", shotPower);
	}
	#endregion

	#region FinishMenu methods

	public void ShowFinishMenu (int shotsMade, float timePassed)
	{
		FinishMenu.SetActive (true);
		HUD.SetActive (false);
		MainMenu.SetActive (false);
		UpdateEndResultDisplay (shotsMade, timePassed);
	}

	private void UpdateEndResultDisplay (int shotsMade, float timePassed)
	{
		EndResultText.text = string.Format ("You needed: {0} shots to finish for {1} seconds", shotsMade, timePassed);
	}
	#endregion
}
