﻿using System.Collections.Generic;
using UnityEngine;

public class TrajectoryRenderer : MonoBehaviour
{
	[SerializeField]
	LineRenderer pathRendrer;

	public void DrawTrajectory (Vector2 startPos, Vector2 direction, float speed, float timePerSegmentInSeconds, float maxTravelDistance)
	{
		var positions = new List<Vector3> ();
		var currentPos = startPos;

		positions.Add (new Vector3 (startPos.x, startPos.y, 0f));

		var traveledDistance = 0.0f;
		while (traveledDistance < maxTravelDistance)
		{
			traveledDistance += speed * timePerSegmentInSeconds;
			var hasHitSomething = TravelTrajectorySegment (currentPos, direction, speed, timePerSegmentInSeconds, positions);
			if (hasHitSomething)
			{
				break;
			}
			var lastPos = currentPos;
			currentPos = positions[positions.Count - 1];
			direction = currentPos - lastPos;
			direction.Normalize ();
		}

		BuildTrajectoryLine (positions);
	}

	private bool TravelTrajectorySegment (Vector2 startPos, Vector2 direction, float speed, float timePerSegmentInSeconds, List<Vector3> positions)
	{
		var newPos2d = startPos + direction * speed * timePerSegmentInSeconds;
		var newPos = new Vector3 (newPos2d.x, newPos2d.y, 0);

		var hasHitSomething = Physics2D.Linecast (startPos, newPos);
		if (hasHitSomething && !hasHitSomething.collider.gameObject.CompareTag ("Player"))
		{
			return true;
		}
		positions.Add (newPos);
		return false;
	}

	private void BuildTrajectoryLine (List<Vector3> positions)
	{
		pathRendrer.positionCount = (positions.Count);
		for (var i = 0; i < positions.Count; ++i)
		{
			pathRendrer.SetPosition (i, positions[i]);
		}
	}
}
